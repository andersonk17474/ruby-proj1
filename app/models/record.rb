class Record < ActiveRecord::Base
  validates :title, presence: true
  validates :category, presence: true
  validates :description, presence: true
  validates :added_by, presence: true
  scope :search, ->(search) {  where ("search_vector @@ to_tsquery('#{search}')") }

end

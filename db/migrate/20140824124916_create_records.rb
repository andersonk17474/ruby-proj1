class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string :title
      t.string :category
      t.text :description
      t.date :created_date

      t.timestamps
    end
  end
end

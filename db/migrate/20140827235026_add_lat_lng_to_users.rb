class AddLatLngToUsers < ActiveRecord::Migration
  def up
    add_column :users, :lat, :float
    add_column :users, :lng, :float
    add_column :users, :street, :string
    add_column :users, :suite, :string
    add_column :users, :city, :string
    add_column :users, :state, :string
    add_column :users, :postal, :string
  end
  
  def down
    remove_column :users, :lat, :float
    remove_column :users, :lng, :float
    remove_column :users, :street, :string
    remove_column :users, :suite, :string
    remove_column :users, :city, :string
    remove_column :users, :state, :string
    remove_column :users, :postal, :string
  end
end

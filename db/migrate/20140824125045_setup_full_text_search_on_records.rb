class SetupFullTextSearchOnRecords < ActiveRecord::Migration
  def up
    execute 'ALTER TABLE RECORDS ADD COLUMN search_vector tsvector'
    execute 'CREATE INDEX search_text ON RECORDS USING gin(search_vector)'
    execute "UPDATE RECORDS 
             SET search_vector = setweight(to_tsvector('pg_catalog.english',coalesce(title,'')),'A') ||
                                 setweight(to_tsvector('pg_catalog.english',coalesce(category,'')),'D') ||
                                 setweight(to_tsvector('pg_catalog.english',coalesce(description,'')),'C')"
                                   
    execute <<-SQL
      CREATE FUNCTION search_trigger() RETURNS trigger AS $$
      begin
        new.search_vector := setweight(to_tsvector('pg_catalog.english',coalesce(new.title,'')),'A') ||
          setweight(to_tsvector('pg_catalog.english',coalesce(new.category,'')),'D')||
          setweight(to_tsvector('pg_catalog.english',coalesce(new.description,'')),'C');
        return new;
      end 
      $$ LANGUAGE plpgsql;
    SQL
    
    execute "CREATE TRIGGER search_vector_update BEFORE INSERT OR UPDATE OF title, category, description ON RECORDS FOR EACH ROW EXECUTE PROCEDURE search_trigger();"          
  end
  
  def down
    # In here we should be reversing what you see above
    # Remove the trigger
    # Remove the index
    # Remove the column on the RECORDS table
    
    execute "ALTER TABLE RECORDS DROP COLUMN search_vector"
    execute "DROP TRIGGER search_vector_update ON RECORDS"
    execute "DROP FUNCTION IF EXISTS search_trigger()"
  end
end

class User < ActiveRecord::Base
  acts_as_mappable :default_units => :miles,
                   :default_formula => :sphere,
                   :distance_field_name => :distance,
                   :lat_column_name => :lat,
                   :lng_column_name => :lng
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable
  
  def update_geodata!
    geo = geocode
    if geo.success
      self.lat, self.lng = geo.lat,geo.lng
      self.save!
    end
  end
  
  def geocode
    geodata = Geokit::Geocoders::MultiGeocoder.geocode(full_address)
    
  end
  
  def full_address
    "#{street} #{suite} #{city} #{state} #{postal}".squish
  end
end

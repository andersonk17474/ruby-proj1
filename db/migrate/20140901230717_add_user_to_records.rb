class AddUserToRecords < ActiveRecord::Migration
  def up
    add_column :records, :added_by, :string
  end
  
  def down
    remove_column :records, :added_by, :string
  end
end



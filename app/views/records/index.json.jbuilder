json.array!(@records) do |record|
  json.extract! record, :id, :title, :category, :description, :created_date
  json.url record_url(record, format: :json)
end
